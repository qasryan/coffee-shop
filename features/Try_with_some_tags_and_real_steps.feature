
@tags @tags2
Feature: Try with some tags and real steps
  <Some interesting description here>

  Scenario: Test me with some real steps
    <Some interesting scenario steps here>
    Given a test step
    When I test with another step
    Then I would test with this step too
    And I would expect a test to render
