@scenario @bitbucket @editor
Feature: Test the bitbucket experience
  As a user in bitbucket
  If I go install the plugin
  I expect to see tag completion
  As well as step completion

  Scenario: A new scenario
    Given a jira instance
    And a scenario installation
    When I create a new feature file
    And I save it
    Then I expect to see step completion and tag completion
