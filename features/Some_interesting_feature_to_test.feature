
Feature: Some interesting feature to test
  As an end user
  When I do something
  I expect something else

  @github
  Scenario Outline: Test upload file to <repo>
    This is a test of uploading
    Given a github issue
    When I click commit
    Then I expect a pull request to be opened in <repo>
    
    Examples:
      | repo |
      | github |